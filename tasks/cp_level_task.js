/**
 * 任务名称
 * name: cp阶段任务
 * 活动时间:  8.21-10.15
 * 定时规则 8-10月每天14,10分执行
 // * cron: 10 14 * 8,9,10 ?
 * 活动地址
 * https://bns.qq.com/cp/a20240821chan/index.html
 */
const {request, obtainTokens, sendNotify, sleep} = require("../common.js");
const tokens = obtainTokens();


const url = "https://comm.ams.game.qq.com/ide/";
const params = {
    iChartId: 319015,
    iSubChartId: 319015,
    sIdeToken: 'FZXLHB',
    e_code: 0,
    g_code: 0,
    eas_url: 'http%3A%2F%2Fbns.qq.com%2Fcp%2Fa20240821chan%2F',
    eas_refer: 'http%3A%2F%2Fnoreferrer%2F%3Freqid%3Dc6b473f5-5d1d-48db-9e41-eef52836a41e%26version%3D27',
    sMiloTag: 'AMS-neo-0823110628-SJbbB5-662924-1063914',
    ams_targetappid: 'wx92a4aa7b8d0ff288',
    isPreengage: 1,
    needGopenid: 1
}

function doReq(token) {
    return request({
        url,
        params: params,
        token,
    }).then((res) => Promise.resolve(res.sMsg || res.msg));
}

(async () => {
    for (let i = 0; i < tokens.length; i++) {
        const token = tokens[i];
        const progress = `(${i + 1}/${tokens.length})`;
        console.log(`【${token.name}】${progress}开始执行`);
        sleep(Math.random() * 1500 + 1500);
        const msg = `${await doReq(token.token)}`;
        console.log(`【${token.name}】` + msg);
        console.log(`【${token.name}】${progress}完毕`);
        sendNotify({
            tokenName: token.name,
            title: `cp阶段任务`,
            body: msg,
        });
    }
})();
