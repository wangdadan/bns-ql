/**
 * 任务名称
 * name: cp每日任务
 * 活动时间:  8.21-10.15
 * 定时规则 8-10月每天14,10分执行
 // * cron: 10 14 * 8,9,10 ?
 * 活动地址
 * https://bns.qq.com/cp/a20240821chan/index.html
 */
const {request, obtainTokens, sendNotify, sleep} = require("../common.js");
const tokens = obtainTokens();


const url = "https://comm.ams.game.qq.com/ide/";
const params = {
    iChartId: 319018,
    iSubChartId: 319018,
    sIdeToken: 'cWMMQk',
    e_code: 0,
    g_code: 0,
    eas_url: 'http%3A%2F%2Fbns.qq.com%2Fcp%2Fa20240821chan%2F',
    eas_refer: 'http%3A%2F%2Fnoreferrer%2F%3Freqid%3D20402404-cac8-4268-af15-80be4f9d8e12%26version%3D27',
    sMiloTag: 'AMS-neo-0823100016-9e0YNR-662924-1063916',
    ams_targetappid: 'wx92a4aa7b8d0ff288',
    iCpType: 1,
    isPreengage: 1,
    needGopenid: 1
}

function doReq(type, token) {
    return request({
        url,
        params: {
            ...params,
            iType: type,
        },
        token,
    }).then((res) => Promise.resolve(res.sMsg || res.msg));
}

(async () => {
    for (let i = 0; i < tokens.length; i++) {
        const token = tokens[i];
        const progress = `(${i + 1}/${tokens.length})`;
        console.log(`【${token.name}】${progress}开始执行`);
        const result = [];
        for (let i = 1; i <= 3; i++) {
            sleep(Math.random() * 1500 + 1500);
            const msg = `第${i}个：${await doReq(i, token.token)}`;
            console.log(`【${token.name}】` + msg);
            result.push(msg);
        }
        const msg = result.join("\n");
        console.log(`【${token.name}】${progress}完毕`);
        sendNotify({
            tokenName: token.name,
            title: `cp每日任务`,
            body: msg,
        });
    }
})();
