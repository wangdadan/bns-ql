/**
 * 任务名称
 * name: 每月1000红石
 * 定时规则 每月1号早上6.30领取
 * cron: 30 6 1 * ?
 * 活动地址
 * https://bns.qq.com/lbact/a20240423lbq39g5/index.html
 */
const {sleep, request, obtainTokens, sendNotify} = require('../common.js')
const tokens = obtainTokens()

const url = 'https://x6m5.ams.game.qq.com/ams/ame/amesvr?sServiceType=neo&iActivityId=636339&sServiceDepartment=group_5&sSDID=357af2f47a55fd64169e1532a7cafa99'
const params = {
    sServiceType: 'neo',
    iActivityId: 636339,
    sServiceDepartment: 'group_5',
    iFlowId: 1032288,
    g_tk: 1842395457,
    sMiloTag: 'AMS-neo-0704095023-cxzui0-636339-1032288',
    e_code: 0,
    g_code: 0,
    eas_url: 'https%253A%252F%252Fbns.qq.com%252Flbact%252Fa20240423lbq39g5%252Findex.html',
    eas_refer: 'https%253A%252F%252Fbns.qq.com%252Fwebplat%252Finfo%252Fnews_version3%252F1298%252F61630%252F61631%252Fm1109%252F202404%252F951343.shtml',
    sPlatId: 0,
    sArea: 0,
    appid: 0,
    isPreengage: 1,
    needGopenid: 1
};

(async () => {
    for (let i = 0; i < tokens.length; i++) {
        const progress = `(${i + 1}/${tokens.length})`
        const token = tokens[i]
        console.log(`【${token.name}】${progress}开始执行`)
        const data = await request({url, params, token: token.token})
        const msg = data.sMsg || data.msg
        sendNotify({
            tokenName: token.name,
            title: '每月红石',
            body: msg
        })
        console.log(`【${token.name}】${progress}开始执行完毕：${msg}`)
        sleep(2000)
    }
})()
