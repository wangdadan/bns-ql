# 获取token

1. 进入活动页面 并且登录后绑定角色
2. 在浏览器中点按键盘`F12`按键(笔记本可能需要`FN+F12`) 启动开发者工具界面
3. 在开发者页面的tab栏中找到`网络(network)`,点击他
4. 在页面中随意点击一次`领取奖励`按钮;  不论成功失败都会出现下图所示的数据
![network](./network.png)
5. 找到并且点击`ide/`这个栏目,点击后会打开新的视图界面
6. 在界面中寻找`请求表头(Request Header)`,并且在其中找到`Cookie`字段;如下图所示
![cookie](./cookie.png)
7. 将`Cookie`后面的一大段文本中寻找 `acctype`,`openid`,`access_token`,`appid`字段,并且将他复制下来
8. 如果你完成了上述步骤,应会得到一段文本,如下所示

```text
acctype=qc; openid=xxx; access_token=xxx; appid=xxx;
```

9. 至此,token获取完毕
